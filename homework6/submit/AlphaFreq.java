import java.util.Comparator;

/**
 * 08722 Data Structures for Application Programmers.
 *
 * Homework Assignment 6
 * Comaprator
 *
 * AlphaFreq
 *
 * Andrew ID: jianrenw
 * @author Jianren Wang
 */

public class AlphaFreq implements Comparator<Word> {
    /**
     * Compare method.
     * @param w1 object 1
     * @param w2 object 2
     * @return int
     */
    public int compare(Word w1, Word w2) {
        if (w1.getWord().compareTo(w2.getWord()) == 0) {
            return w1.getFrequency() - w2.getFrequency();
        }
        return w1.getWord().compareTo(w2.getWord());
    }
}
