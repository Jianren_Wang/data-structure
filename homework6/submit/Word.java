import java.util.Set;
import java.util.HashSet;

/**
 * 08722 Data Structures for Application Programmers.
 *
 * Homework Assignment 6
 * Word class
 *
 * Andrew ID: jianrenw
 * @author Jianren Wang
 */

public class Word implements Comparable<Word> {
    /**
     * word.
     */
    private String word;
    /**
     * index.
     */
    private Set<Integer> index;
    /**
     * frequency.
     */
    private int frequency;
    /**
     * Constructor with input.
     * @param input input string
     */
    public Word(String input) {
        if (input == null || input.equals("") || !isWord(input)) {
            return;
        }
        index = new HashSet<Integer>();
        frequency = 1;
        word = input;
    }
    /**
     * Set word.
     * @param input input word
     */
    public void setWord(String input) {
        if (input == null || input.equals("") || !isWord(input)) {
            return;
        }
        word = input;
    }
    /**
     * Set index.
     * @param input index
     */
    public void setIndex(Set<Integer> input) {
        index = new HashSet<Integer>(input);
    }
    /**
     * Set frequency.
     * @param input frequency
     */
    public void setFrequency(int input) {
        frequency = input;
    }
    /**
     * Add to index.
     * @param line line number
     */
    public void addToIndex(Integer line) {
        index.add(line);
    }
    /**
     * Get word.
     * @return word
     */
    public String getWord() {
        return word;
    }
    /**
     * Get frequency.
     * @return frequency
     */
    public int getFrequency() {
        return frequency;
    }
    /**
     * Get index.
     * @return index
     */
    public Set<Integer> getIndex() {
        return new HashSet<Integer>(index);
    }
    /**
     * Compare to method.
     * @param newWord word to be compared
     * @return comparison
     */
    @Override
    public int compareTo(Word newWord) {
        return word.compareTo(newWord.word);
    }
    /**
     * To string method.
     * @return string display
     */
    @Override
    public String toString() {
        StringBuilder display = new StringBuilder();
        display.append(word);
        display.append(" ");
        display.append(frequency);
        display.append(" ");
        display.append("[");
        int count = 0;
        for (Integer i : index) {
            display.append(i);
            if (count < index.size() - 1) {
                display.append(", ");
            }
            count++;
        }
        display.append("]");
        return display.toString();
    }
    /**
     * Validate the legality of input string.
     * @param w input word to be validated
     * @return whether the word is validated or not
     */
    private boolean isWord(String w) {
        return w.matches("[a-zA-Z]+");
    }
}
