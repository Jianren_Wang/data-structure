import java.util.Comparator;
import java.util.Iterator;
import java.util.Stack;

/**
 * 08722 Data Structures for Application Programmers.
 *
 * Homework Assignment 6
 * BST
 *
 * Binary Search Tree Class
 *
 * Andrew Id: jianrenw
 * @author Jianren Wang
 */

/**
 * Binary Search Tree Class.
 * @param <T> anytype
 */
public class BST<T extends Comparable<T>> implements Iterable<T>, BSTInterface<T> {
    /**
     * root.
     */
    private Node<T> root;
    /**
     * comparator.
     */
    private Comparator<T> comparator;
    /**
     * constructor.
     */
    public BST() {
        this(null);
    }
    /**
     * Comstructor with comparator.
     * @param comp comaprator
     */
    public BST(Comparator<T> comp) {
        comparator = comp;
        root = null;
    }
    /**
     * Comparator class.
     * @return comparator
     */
    public Comparator<T> comparator() {
        return comparator;
    }
    /**
     * Get root.
     * @return root
     */
    public T getRoot() {
        if (root == null) {
            return null;
        } else {
            return root.data;
        }
    }
    /**
     * Get height.
     * @return height
     */
    public int getHeight() {
        if (root == null) {
            return 0;
        } else {
            return getHeightHelper(root);
        }
    }
    /**
     * Helper function to get the height.
     * Base case: Current Node is null or both left and right is null.
     * Recursive Case: Increased max left/right height by 1.
     * @param node current node
     * @return the height
     */
    private int getHeightHelper(Node<T> node) {
        // Base case
        if (node == null || (node.left == null && node.right == null)) {
            return 0;
        }

        // Recursive case
        int leftHeight  = getHeightHelper(node.left);
        int rightHeight = getHeightHelper(node.right);
        return 1 + Math.max(leftHeight, rightHeight);
    }
    /**
     * Get word number.
     * @return word number
     */
    public int getNumberOfNodes() {
        return getNumOfNodesHelper(root);
    }
    /**
     * Helper function to get the number of nodes.
     * Base case: Current node is null.
     * Recursive case: Count left and right, then sum by 1.
     * @param node current node
     * @return number of nodes
     */
    private int getNumOfNodesHelper(Node<T> node) {
        // Base case
        if (node == null) {
            return 0;
        }
        // Recursive case
        int leftNumNodes  = getNumOfNodesHelper(node.left);
        int rightNumNodes = getNumOfNodesHelper(node.right);
        return 1 + leftNumNodes + rightNumNodes;
    }
    /**
     * Search Method.
     * @param toSearch data to search
     * @return if found, then return the node data, else return null.
     */
    @Override
    public T search(T toSearch) {
        return searchRecursive(toSearch, root);
    }
    /**
     * Helper function to search.
     * Base case: Current Node is null.
     * Recursive Case: Current Node is not null.
     * If current node is wanted, return.
     * If not, search left and right tree.
     * @param toSearch data to search
     * @param node current Node
     * @return if found, then return the node data, else return null.
     */
    private T searchRecursive(T toSearch, Node<T> node) {
        // Base case
        if (node == null) {
            return null;
        }
        if (node.data.compareTo(toSearch) == 0) {
            // Base case
            return node.data;
        } else if (toSearch.compareTo(node.data) < 0) {
            // Recursive case
            return searchRecursive(toSearch, node.left);
        } else {
            // Recursive case
            return searchRecursive(toSearch, node.right);
        }
    }   
    /**
     * Insert Method.
     * @param toInsert data to be inserted
     */
    @Override
    public void insert(T toInsert) {
        if (root == null) {
            root = new Node<T>(toInsert);
            return;
        }
        insertHelper(root, toInsert, null);
    }
    /**
     * Helper function to insert.
     * Base case: find the insertion place.
     * Recursive case: the current node is not null, do left or right.
     * @param toInsert data to insert
     * @param current current node
     * @param parent parent
     */
    private void insertHelper(Node<T> current, T toInsert, Node<T> parent) {
        // Base case
        if (current == null) {
            if (toInsert.compareTo(parent.data) == 0) {
                return;
            } else if (toInsert.compareTo(parent.data) < 0) {
                parent.left = new Node<T>(toInsert);
            } else {
                parent.right = new Node<T>(toInsert);
            }
        } else {
            // Recursive case
            if (toInsert.compareTo(current.data) == 0) {
                return;
            } else if (toInsert.compareTo(current.data) < 0) {
                insertHelper(current.left, toInsert, current);
            } else {
                insertHelper(current.right, toInsert, current);
            }
        }
    }
    /**
     * Iterator.
     * @return the InOrderIterator
     */
    @Override
    public Iterator<T> iterator() {
        return new InOrderIterator();
    }
    /**
     * InOrderIterator class.
     */
    private class InOrderIterator implements Iterator<T> {
        /**
         * Stack for InOrderIterator.
         */
        private Stack<Node<T>> stack;

        /**
         * Constructor of InOrderIterator.
         */
        public InOrderIterator() {
            stack = new Stack<Node<T>>();
            Node<T> node = root;
            while (node != null) {
                stack.push(node);
                node = node.left;
            }
        }
        /**
         * If the iterator has next node or not.
         * @return If the iterator has next node or not
         */
        @Override
        public boolean hasNext() {
            return !stack.isEmpty();
        }
        /**
         * Next node of InOrderIterator.
         * @return next node of InOrderIterator
         */
        @Override
        public T next() {
            Node<T> node = stack.pop();
            Node<T> curr = node;
            if (curr.right != null) {
                curr = curr.right;
                while (curr != null) {
                    stack.push(curr);
                    curr = curr.left;
                }
            }
            return (T) node.data;
        }
    }

    /**
     * Node class.
     * @param <T> Anytype
     */
    private static class Node<T> {
        /**
         * data.
         */
        private T data;
        /**
         * left.
         */
        private Node<T> left;
        /**
         * right.
         */
        private Node<T> right;
        /**
         * constructor.
         * @param d data.
         */
        Node(T d) {
            this(d, null, null);
        }
        /**
         * constructor.
         * @param d data
         * @param l left
         * @param r right
         */
        Node(T d, Node<T> l, Node<T> r) {
            data = d;
            left = l;
            right = r;
        }
        /**
         * toString method.
         */
        @Override
        public String toString() {
            return data.toString();
        }
    }
}

