import java.util.Comparator;

/**
 * 08722 Data Structures for Application Programmers.
 *
 * Homework Assignment 6
 * Comaprator
 *
 * Frequency
 *
 * Andrew ID: jianrenw
 * @author Jianren Wang
 */

public class Frequency implements Comparator<Word> {
	/**
     * Compare method.
     * @param w1 object 1
     * @param w2 object 2
     * @return int
     */
    public int compare(Word w1, Word w2) {
        return w2.getFrequency() - w1.getFrequency();
    }
}
