import java.util.ArrayList;
import java.util.Comparator;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.Scanner;
/**
 * 08722 Data Structures for Application Programmers.
 *
 * Homework Assignment 6
 * Index
 *
 * Index Class
 *
 * Andrew Id: jianrenw
 * @author Jianren Wang
 */

public class Index {
    /**
     * Constructor with fileName.
     * @param fileName file name
     * @return BST
     */
    public BST<Word> buildIndex(String fileName) {
        if (fileName == null) {
            return null;
        }
        BST<Word> myBST = new BST<Word>();
        Scanner scanner = null;
        try {
            int lineNumber = 0;
            scanner = new Scanner(new File(fileName), "latin1");
            while (scanner.hasNextLine()) {
                lineNumber++;
                String line = scanner.nextLine();
                String[] words = line.split("\\W");
                for (String word : words) {
                    if (isWord(word)) {
	                    Word w = new Word(word);
	                    w.addToIndex(lineNumber);
	                    Word searchResult = myBST.search(w);
	                    if (searchResult == null) {
	                        myBST.insert(w);
	                    } else {
	                        searchResult.addToIndex(lineNumber);
	                        searchResult.setFrequency(searchResult.getFrequency() + 1);
	                    }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("File not found.");
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
        return myBST;
    }
    /**
     * Constructor with filename and comparator.
     * @param fileName file name
     * @param comparator comparator
     * @return BST
     */
    public BST<Word> buildIndex(String fileName, Comparator<Word> comparator) {
        if (fileName == null) {
            return null;
        }
        BST<Word> myBST = new BST<Word>(comparator);
        Scanner scanner = null;
        try {
            int lineNumber = 0;
            scanner = new Scanner(new File(fileName), "latin1");
            while (scanner.hasNextLine()) {
                lineNumber++;
                String line = scanner.nextLine();
                String[] words = line.split("\\W");
                for (String word : words) {
                    if (isWord(word)) {
	                    if (comparator instanceof IgnoreCase) {
	                        word = word.toLowerCase();
	                    }
	                    Word w = new Word(word);
	                    w.addToIndex(lineNumber);
	                    Word searchResult = myBST.search(w);
	                    if (searchResult == null) {
	                        myBST.insert(w);
	                    } else {
	                        searchResult.addToIndex(lineNumber);
	                        searchResult.setFrequency(searchResult.getFrequency() + 1);
	                    }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("File not found.");
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
        return myBST;
    }
    /**
     * Constructor with list and comparator.
     * @param list list
     * @param comparator comparator
     * @return BST
     */
    public BST<Word> buildIndex(ArrayList<Word> list, Comparator<Word> comparator) {
        BST<Word> myBST = new BST<Word>(comparator);
        if (list == null) {
            return myBST;
        }
        if (comparator instanceof IgnoreCase) {
            for (Word word:list) {
                Word w = new Word(word.getWord().toLowerCase());
                w.setFrequency(word.getFrequency());
                w.setIndex(word.getIndex());
                myBST.insert(w);
            }
        } else {
            for (Word word:list) {
                Word w = new Word(word.getWord());
                w.setFrequency(word.getFrequency());
                w.setIndex(word.getIndex());
                myBST.insert(w);
            }
        }
        return myBST;
    }
    /**
     * Sort by alpha.
     * @param tree BST
     * @return list of words
     */
    public ArrayList<Word> sortByAlpha(BST<Word> tree) {
        /*
         * Even though there should be no ties with regard to words in BST,
         * in the spirit of using what you wrote, 
         * use AlphaFreq comparator in this method.
         */
        ArrayList<Word> list = new ArrayList<Word>();
        if (tree == null) {
            return list;
        }
        for (Word word : tree) {
            list.add(word);
        }
        Collections.sort(list, new AlphaFreq());
        return list;
    }
    /**
     * Sort by Frequency.
     * @param tree BST
     * @return list of words
     */
    public ArrayList<Word> sortByFrequency(BST<Word> tree) {
        ArrayList<Word> list = new ArrayList<Word>();
        if (tree == null) {
            return list;
        }
        for (Word word : tree) {
            list.add(word);
        }
        Collections.sort(list, new Frequency());
        return list;
    }
    /**
     * Get a list of words with highest frequency.
     * @param tree BST
     * @return list of words
     */
    public ArrayList<Word> getHighestFrequency(BST<Word> tree) {
        ArrayList<Word> list = new ArrayList<Word>();
        if (tree == null) {
            return null;
        }
        ArrayList<Word> t = sortByFrequency(tree);
        Word maxFreq = t.get(0);
        list.add(maxFreq);
        for (int i = 1; i < t.size(); i++) {
            if (t.get(i).getFrequency() < maxFreq.getFrequency()) {
                break;
            }
            list.add(t.get(i));
        }
        return list;
    }
    /**
     * Validate the legality of input string.
     * @param word input word to be validated
     * @return whether the word is validated or not
     */
    private boolean isWord(String word) {
        return word.matches("[a-zA-Z]+");
    }
}
