import java.util.Comparator;

/**
 * 08722 Data Structures for Application Programmers.
 *
 * Homework Assignment 6
 * Comaprator
 *
 * Ignorecase
 *
 * Andrew ID: jianrenw
 * @author Jianren Wang
 */

public class IgnoreCase implements Comparator<Word> {
	/**
     * Compare method.
     * @param w1 object 1
     * @param w2 object 2
     * @return int (0,positive,negative)
     */
    public int compare(Word w1, Word w2) {
        return w1.getWord().toLowerCase().compareTo(w2.getWord().toLowerCase());
    }
}
