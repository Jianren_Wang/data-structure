import java.util.ArrayDeque;
import java.util.LinkedList;
/**
 * 08722 Data Structures for Application Programmers.
 *
 * Homework Assignment 2 Solve Josephus problem
 * with different data structures
 * and different algorithms and compare running times
 *
 * Andrew ID: jianrenw
 * @author Jianren Wang
 * Conclusion: When choosing method, there size and rotation are main considerations.
 * Assuming the size is very large as mentioned in handout.
 * When the rotation is small, the first two methods are better.
 * When rotation is close to size (very large), the third method is better.
 * Under most conditions, the first method is better than the second one.
 * WHY?
 * For each removement, the first two methods require two operations (add and remove),
 * the third method requires one operation (delete).
 * For each rotation, the first two methods will apply removement nearly rotation times,
 * thus when rotation is large, the first two methods are worse.
 * However, when the rotaion is small, more time is needed to find an index using linked list.
 * Thus, the third method is worse.
 */
public class Josephus {

    /**
     * Uses ArrayDeque class as Queue/Deque to find the survivor's position.
     * @param size Number of people in the circle that is bigger than 0
     * @param rotation Elimination order in the circle. The value has to be greater than 0
     * @return The position value of the survivor
     */
    public int playWithAD(int size, int rotation) {
        // TODO your implementation here
        if (size < 1 || rotation < 1) {
            throw new RuntimeException("Size and rotation should be larger than 0.");
        }
        ArrayDeque<Integer> d = new ArrayDeque<Integer>(size);
        for (int i = 1; i < size + 1; i++) {
            d.addLast(i);
        }
        while (d.size() != 1) {
            int index = (rotation - 1) % d.size();
            for (int i = 0; i < index; i++) {
                int tmp = d.removeFirst();
                d.addLast(tmp);
            }
            d.removeFirst();
        }
        return d.peek();
    }

    /**
     * Uses LinkedList class as Queue/Deque to find the survivor's position.
     * @param size Number of people in the circle that is bigger than 0
     * @param rotation Elimination order in the circle. The value has to be greater than 0
     * @return The position value of the survivor
     */
    public int playWithLL(int size, int rotation) {
        // TODO your implementation here
        if (size < 1 || rotation < 1) {
            throw new RuntimeException("Size and rotation should be larger than 0.");
        }
        LinkedList<Integer> d = new LinkedList<Integer>();
        for (int i = 1; i < size + 1; i++) {
            d.addLast(i);
        }
        while (d.size() != 1) {
            int index = (rotation - 1) % d.size();
            for (int i = 0; i < index; i++) {
                int tmp = d.removeFirst();
                d.addLast(tmp);
            }
            d.removeFirst();
        }
        return d.peek();
    }

    /**
     * Uses LinkedList class to find the survivor's position.
     * However, do NOT use the LinkedList as Queue/Deque
     * Instead, use the LinkedList as "List"
     * That means, it uses index value to find and remove a person to be executed in the circle
     *
     * Note: Think carefully about this method!!
     * When in doubt, please visit one of the office hours!!
     *
     * @param size Number of people in the circle that is bigger than 0
     * @param rotation Elimination order in the circle. The value has to be greater than 0
     * @return The position value of the survivor
     */
    public int playWithLLAt(int size, int rotation) {
        // TODO your implementation here
        if (size < 1 || rotation < 1) {
            throw new RuntimeException("Size and rotation should be larger than 0.");
        }
        LinkedList<Integer> d = new LinkedList<Integer>();
        for (int i = 1; i < size + 1; i++) {
            d.addLast(i);
        }
        int index = (rotation - 1) % d.size();
        while (d.size() != 1) {
            d.remove(index);
            index = (index + rotation - 1) % d.size();
        }
        return d.peek();
    }
}
