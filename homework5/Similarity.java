import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 08722 Data Structures for Application Programmers.
 *
 * Homework Assignment 5
 * Similarity Implemenation using HashMap
 *
 * Andrew ID: jianrenw
 * @author Jianren Wang
 */

public class Similarity {
    /**
     * Use HashMap to store frequency of words.
     * Purpose: HashMap structure can insert and retrieve frequency of words in constant time.
     */
    private Map<String, BigInteger> map;
    /**
     * Number of lines.
     */
    private int numLines;
    /**
     * Number of words.
     */
    private BigInteger numWords;

    /**
     * Constructor with input String.
     * @param str input String
     */
    public Similarity(String str) {
        map = new HashMap<String, BigInteger>();
        numLines = 0;
        numWords = BigInteger.ZERO;
        if (str == null || str.length() == 0) {
            return;
        }
        numLines++;
        String[] words = str.split("\\W");
        for (String w: words) {
            if (isWord(w)) {
                numWords = numWords.add(BigInteger.ONE);
                w = w.toLowerCase();
                if (map.containsKey(w)) {
                    map.put(w, map.get(w).add(BigInteger.ONE));
                } else {
                    map.put(w, BigInteger.ONE);
                }
            }
        }
    }


    /**
     * Constructor with input File.
     * @param file input File
     */
    public Similarity(File file) {
        map = new HashMap<String, BigInteger>();
        numLines = 0;
        numWords = BigInteger.ZERO;
        if (file == null) {
            return;
        }
        Scanner scanner = null;
        try {
            scanner = new Scanner(file, "latin1");
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] words = line.split("\\W");
                for (String w : words) {
                    if (isWord(w)) {
                        w = w.toLowerCase();
                        numWords = numWords.add(BigInteger.ONE);
                        if (map.containsKey(w)) {
                            map.put(w, map.get(w).add(BigInteger.ONE));
                        } else {
                            map.put(w, BigInteger.ONE);
                        }
                    }
                }
                numLines++;
            }
        } catch (FileNotFoundException e) {
            System.err.println("Cannot find the file");
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
    }

     /**
     * Line Number.
     * @return line number
     */
    public int numOfLines() {
        return numLines;
    }

    /**
     * Word Number.
     * @return Word Number
     */
    public BigInteger numOfWords() {
        return numWords;
    }

    /**
     * Number of words without duplicates.
     * @return number of words without duplicates
     */
    public int numOfWordsNoDups() {
        return map.size();
    }

    /**
     * Compute Euclidean Norm.
     * @return Euclidean Norm
     */
    public double euclideanNorm() {
        if (map == null) {
            return 0.0;
        }
        return calEuclideanNorm(map);
    }

     /**
     * Calculate the Euclidean Norm for the input map.
     * @param inputMap input map
     * @return the Euclidean Norm for the input map
     */
    private double calEuclideanNorm(Map<String, BigInteger> inputMap) {
        BigInteger norm = BigInteger.ZERO;
        for (Map.Entry<String, BigInteger> entry : inputMap.entrySet()) {
            norm = norm.add(entry.getValue().multiply(entry.getValue()));
        }
        return Math.sqrt(norm.doubleValue());
    }

     /**
     * Calculate dot product.
     * Fot each time, this function will loop through all entries in the map, which is O(n)
     * For each iteration, only search is used, which is O(1), in HashMap structure.
     * So the total complexicity is O(n), which will not fall into quadratic on average.
     * @param inputMap input map
     * @return dot product of the two maps
     */
    public double dotProduct(Map<String, BigInteger> inputMap) {
        if (map == null || inputMap == null) {
            return 0.0;
        }
        BigInteger dot = BigInteger.ZERO;
        for (Map.Entry<String, BigInteger> entry : map.entrySet()) {
            String key = entry.getKey();
            BigInteger value = entry.getValue();
            if (inputMap.get(key) != null) {
                dot = dot.add(value.multiply(inputMap.get(key)));
            }
        }
        return dot.doubleValue();
    }

     /**
     * Compute the cosine distance between the input map and current map.
     * @param inputMap input map
     * @return cosine distance
     */
    public double distance(Map<String, BigInteger> inputMap) {
        if (map == null && inputMap == null) {
            return 0.0;
        }
        if (map == null || inputMap == null) {
            return Math.PI / 2;
        }
        if (euclideanNorm() == 0 || calEuclideanNorm(inputMap) == 0) {
            return Math.PI / 2;
        }

        double dis = dotProduct(inputMap) / euclideanNorm() / calEuclideanNorm(inputMap);
        if (dis > 1) {
            dis = 1.0;
        }
        return Math.acos(dis);
    }

    /**
     * Get the map.
     * @return a deep copy of the map
     */
    public Map<String, BigInteger> getMap() {
        return new HashMap<String, BigInteger>(map);
    }

    /**
     * Validate the legality of input string.
     * @param word input word to be validated
     * @return whether the word is validated or not
     */
    private boolean isWord(String word) {
        return word.matches("[a-zA-Z]+");
    }
}
