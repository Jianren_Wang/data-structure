/**
 * 08722 Data Structures for Applications Programmers.
 *
 * Homework 3 SortedLinkedList Implementation with Recursion
 *
 * Basic List implementation
 *
 * Andrew Id: jianrenw
 * @author Jianren Wang
 */

public class SortedLinkedList implements MyListInterface {
    /**
     * Define Node.
     */
    private static class Node {
        /**
         * data.
         */
        private String data;
        /**
         * reference to the next node.
         */
        private Node next;

        /**
         * Constructor with data and next node.
         * @param d data for the node
         * @param n reference to the next node
         */
        Node(String d, Node n) {
            data = d;
            next = n;
        }
    }
    /**
     * head node.
     */
    private Node head;

    /**
     * Constructor without input.
     */
    public SortedLinkedList() {
        head = null;
    }

    /**
     * Constructor with input.
     * @param strArray input String array
     */
    public SortedLinkedList(String[] strArray) {
        if (strArray == null || strArray.length == 0) {
            head = null;
            return;
        }
        constructorRecurser(strArray, 0);
    }

    /**
     * constructorRecurser, add sub array.
     * @param strArray input unsorted sub array
     * @param i index of the array
     */
    private void constructorRecurser(String[] strArray, int i) {
        //Base
        if (i == strArray.length) {
            return;
        }
        //Recursive
        add(strArray[i]);
        constructorRecurser(strArray, i + 1);
    }

    /**
     * Inserts a new String.
     * No duplicates allowed and maintain the order in ascending order.
     * @param str String to be added
     */
    @Override
    public void add(String str) {
        if (str == null || str.length() == 0 || !str.matches("[a-zA-Z]+")) {
            return;
        }
        // If the linklist is empty, just add
        if (head == null) {
            head = new Node(str, null);
            return;
        }
        // If the head is larger, just add to head
        if (head.data.compareTo(str) > 0) {
            head = new Node(str, head);
            return;
        }
        // Duplicate
        if (head.data.compareTo(str) == 0) {
            return;
        }
        addRecurser(head, str);
    }

    /**
     * Recurser, inserts a new String.
     * No duplicates allowed and maintain the order in ascending order.
     * @param prev the node which will be added after
     * @param str String to be added
     */
    private void addRecurser(Node prev, String str) {
        // Base: Add to the end
        if (prev.next == null) {
            prev.next = new Node(str, null);
            return;
        }
        // Base: Duplicate
        if (prev.next.data.equals(str)) {
            return;
        }
        // Base: Add in the mid
        if (prev.next.data.compareTo(str) > 0) {
            Node tmp = new Node(str, prev.next);
            prev.next = tmp;
            return;
        }
        // Recursive
        addRecurser(prev.next, str);
    }

    /**
     * Checks the size (number of data items) of the list.
     * @return the size of the list
     */
    @Override
    public int size() {
        return sizeRecurser(head);
    }

    /**
     * Recurser, checks the size (number of data items) of the list.
     * @param curr current node
     * @return the size of the list
     */
    private int sizeRecurser(Node curr) {
        if (curr == null) {
            return 0;
        }
        return 1 + sizeRecurser(curr.next);
    }

    /**
     * Displays the values of the list.
     */
    @Override
    public void display() {
        if (head == null) {
            System.out.println("[]");
            return;
        }
        String dis = displayRecurser(head, "[");
        System.out.println(dis + "]");
    }

    /**
     * Recurser, displays the values of the list.
     * @param curr current node
     * @param str string to be displayed
     * @return the size of the list
     */
    private String displayRecurser(Node curr, String str) {
        if (curr == null) {
            return str;
        }
        if (curr.next == null) {
            str += curr.data;
        } else {
            str += curr.data + ", ";
        }
        return displayRecurser(curr.next, str);
    }

    /**
     * Returns true if the key value is in the list.
     * @param key String key to search
     * @return true if found, false if not found
     */
    @Override
    public boolean contains(String key) {
         if ((key == null) || (key.length() == 0)) {
            return false;
        }
        return containsRecurser(head, key);
    }

    /**
     * Recurser, returns true if the key value is in the list.
     * @param curr current node
     * @param key String key to search
     * @return true if found, false if not found
     */
    private boolean containsRecurser(Node curr, String key) {
        if (curr == null) {
            return false;
        }
        if (curr.data.equals(key)) {
            return true;
        }
        return containsRecurser(curr.next, key);
    }

    /**
     * Returns true is the list is empty.
     * @return true if it is empty, false if it is not empty
     */
    @Override
    public boolean isEmpty() {
        return head == null;
    }

    /**
     * Removes and returns the first String object of the list.
     * @return String object that is removed. If the list is empty, returns null
     */
    @Override
    public String removeFirst() {
        if (head == null) {
            return null;
        }
        String first = head.data;
        head = head.next;
        return first;
    }

    /**
     * Removes and returns String object at the specified index.
     * @param index index to remove String object
     * @return String object that is removed
     */
    @Override
    public String removeAt(int index) {
        if (index < 0) {
            throw new RuntimeException("Index must be non-negative.");
        }
        if (head == null) {
            throw new RuntimeException("This SortedLinkedList is empty.");
        }
        if (index >= size()) {
            throw new RuntimeException("Exceed size.");
        }
        if (index == 0) {
            return removeFirst();
        }
        return removeAtRecurser(head, index - 1);
    }

    /**
     * Recurser, removes and returns String object at the specified index.
     * @param prev the node which will be deleted after
     * @param index index to remove String object
     * @return String object that is removed
     */
    private String removeAtRecurser(Node prev, int index) {
        // Remove next node
        if (index == 0) {
            String str = prev.next.data;
            prev.next = prev.next.next;
            return str;
        }
        return removeAtRecurser(prev.next, index - 1);
    }
}
