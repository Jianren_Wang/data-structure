/**
 * 08722 Data Structures for Application Programmers.
 *
 * Homework Assignment 4
 * HashTable Implementation with linear probing
 *
 * Andrew ID: Jianren Wang
 * @author jianrenw
 */
public class MyHashTable implements MyHTInterface {
    /**
     * Default load factor.
     */
    private static final double DEFAULT_LOAD_FACTOR = 0.5;
    /**
     * Default capacity.
     */
    private static final int DEFAULT_CAPACITY = 10;
    /**
     * Data array.
     */
    private DataItem[] hashArray;
    /**
     * Number of keys.
     */
    private int nKeys;
    /**
     * Number of collisions.
     */
    private int nCollisions;
    /**
     * Deleted items.
     */
    private static final DataItem DELETED = new DataItem("#DEL#");


    // TODO implement constructor with no initial capacity
    /**
     * Contructor with no initial capacity.
     */
    MyHashTable() {
        hashArray = new DataItem[DEFAULT_CAPACITY];
        nKeys = 0;
        nCollisions = 0;
    }

    // TODO implement constructor with initial capacity
    /**
     * Contructor with initial capacity.
     * @param init initial capacity
     */
    MyHashTable(int init) {
        if (init <= 0) {
            throw new RuntimeException("The initial capacity should be larger than 0.");
        }
        hashArray = new DataItem[init];
        nKeys = 0;
        nCollisions = 0;
    }
    // TODO implement required methods
    /**
     * Inserts a new String value (word).
     * Frequency of each word to be stored too.
     * @param value String value to add
     */
    @Override
    public void insert(String value) {
        if (value == null || value.length() == 0 || !value.matches("[a-zA-Z]+")) {
            return;
        }

        int hashVal = hashFunc(value);
        int prevHashVal = hashVal;

        while (hashArray[hashVal] != null && hashArray[hashVal] != DELETED) {
            if (hashArray[hashVal].value.equals(value)) {
                hashArray[hashVal].frequency++;
                return;
            }
            hashVal++;
            // Wrap around
            hashVal = hashVal % hashArray.length;
        }

        for (int i = 0; i < hashArray.length; i++) {
            if (hashArray[i] != null && hashArray[i] != DELETED && hashFunc(hashArray[i].value) == prevHashVal) {
                nCollisions++;
                break;
            }
        }

        hashArray[hashVal] = new DataItem(value);
        nKeys++;
        if ((1.0 * nKeys / hashArray.length) > DEFAULT_LOAD_FACTOR) {
            rehash();
        }
    }
    /**
     * Returns the size, number of items, of the table.
     * @return the number of items in the table
     */
    @Override
    public int size() {
        return nKeys;
    }
    /**
     * Displays the values of the table.
     * If an index is empty, it shows **
     * If previously existed data item got deleted, then it should show #DEL#
     */
    @Override
    public void display() {
        StringBuilder dis = new StringBuilder();
        for (int i = 0; i < hashArray.length; i++) {
            if (hashArray[i] == null) {
                dis.append("**");
            } else if (hashArray[i] == DELETED) {
                dis.append(hashArray[i].value);
            } else {
                dis.append("[").append(hashArray[i].value).append(", ").append(hashArray[i].frequency).append("]");
            }
            dis.append(" ");
        }
        System.out.println(dis.toString().trim());
    }
    /**
     * Returns true if value is contained in the table.
     * @param key String key value to search
     * @return true if found, false if not found.
     */
    @Override
    public boolean contains(String key) {
        if (key == null || key.length() == 0 || !key.matches("[a-zA-Z]+")) {
            return false;
        }
        int hashVal = hashFunc(key);
        int count = 0;
        while (hashArray[hashVal] != null && count < hashArray.length) {
            if (hashArray[hashVal].value.equals(key)) {
                return true; // found
            }
            count++;
            hashVal++;
            // Wrap around
            hashVal = hashVal % hashArray.length;
        }
        return false; // cannot find
    }
    /**
     * Returns the number of collisions in relation to insert and rehash.
     * When rehashing process happens, the number of collisions should be properly updated.
     *
     * The definition of collision is "two different keys map to the same hash value."
     * Be careful with the situation where you could overcount.
     * Try to think as if you are using separate chaining.
     * "How would you count the number of collisions?" when using separate chaining.
     * @return number of collisions
     */
    @Override
    public int numOfCollisions() {
        return nCollisions;
    }
    /**
     * Returns the hash value of a String.
     * Assume that String value is going to be a word with all lowercase letters.
     * @param value value for which the hash value should be calculated
     * @return int hash value of a String
     */
    @Override
    public int hashValue(String value) {
        return hashFunc(value);
    }
    /**
     * Returns the frequency of a key String.
     * @param key string value to find its frequency
     * @return frequency value if found. If not found, return 0
     */
    @Override
    public int showFrequency(String key) {
        if (key == null || key.length() == 0 || !key.matches("[a-zA-Z]+")) {
            return 0;
        }

        int hashVal = hashFunc(key) % hashArray.length;
        int count = 0;
        while (hashArray[hashVal] != null && count < hashArray.length) {
            if (hashArray[hashVal].value.equals(key)) {
                return hashArray[hashVal].frequency; // found
            }
            hashVal++;
            // Wrap around
            hashVal = hashVal % hashArray.length;
            count++;
        }
        return 0; // cannot find
    }
    /**
     * Removes and returns removed value.
     * @param key String to remove
     * @return value that is removed. If not found, return null
     */
    @Override
    public String remove(String key) {
        if (key == null || key.length() == 0 || !key.matches("[a-zA-Z]+")) {
            return null;
        }

        int hashVal = hashFunc(key) % hashArray.length;
        int count = 0;
        while (hashArray[hashVal] != null && count < hashArray.length) {
            if (hashArray[hashVal].value.equals(key)) {
                String k = hashArray[hashVal].value;
                hashArray[hashVal] = DELETED; // delete
                nKeys--;
                return k; // found
            }
            hashVal++;
            hashVal = hashVal % hashArray.length;
            count++;
        }
        return null; // cannot find
    }
    /**
     * Instead of using String's hashCode, you are to implement your own here.
     * You need to take the table length into your account in this method.
     *
     * In other words, you are to combine the following two steps into one step.
     * 1. converting Object into integer value
     * 2. compress into the table using modular hashing (division method)
     *
     * Helper method to hash a string for English lowercase alphabet and blank,
     * we have 27 total. But, you can assume that blank will not be added into
     * your table. Refer to the instructions for the definition of words.
     *
     * For example, "cats" : 3*27^3 + 1*27^2 + 20*27^1 + 19*27^0 = 60,337
     *
     * But, to make the hash process faster, Horner's method should be applied as follows;
     *
     * var4*n^4 + var3*n^3 + var2*n^2 + var1*n^1 + var0*n^0 can be rewritten as
     * (((var4*n + var3)*n + var2)*n + var1)*n + var0
     *
     * Note: You must use 27 for this homework.
     *
     * However, if you have time, I would encourage you to try with other
     * constant values than 27 and compare the results but it is not required.
     * @param input input string for which the hash value needs to be calculated
     * @return int hash value of the input string
     */
    private int hashFunc(String input) {
        // TODO implement this
        int hashVal = 0;
        input = input.toLowerCase();
        for (int i = 0; i < input.length(); i++) {
            hashVal = (hashVal * 27 + (input.charAt(i) - 'a' + 1)) % hashArray.length;
        }
        return hashVal;
    }
    /**
     * Rehash the current hashtable so that the load factor stays within (less than or equal to) 0.5 at any point.
     */
    private void rehash() {
        // TODO implement this
        int newSize = prime(hashArray.length * 2);
        if (newSize == -1) {
            throw new RuntimeException("Overflow.");
        }
        System.out.println("Rehashing " + nKeys + " items, new length is " + newSize);
        DataItem[] tmp = hashArray;
        hashArray = new DataItem[newSize];
        nCollisions = 0;
        for (int i = 0; i < tmp.length; i++) {
            if (tmp[i] == null || tmp[i] == DELETED) {
                continue;
            }
            int hashVal = hashFunc(tmp[i].value);
            for (int j = 0; j < hashArray.length; j++) {
                if (hashArray[j] != null && hashFunc(hashArray[j].value) == hashVal) {
                    nCollisions++;
                    break;
                }
            }
            while (hashArray[hashVal] != null) {
                hashVal++;
                hashVal = hashVal % hashArray.length;
            }
            hashArray[hashVal] = tmp[i];
        }
    }
    /**
     * static nested class.
     */
    private static class DataItem {
        /**
         * String value.
         */
        private String value;
        /**
         * String value's frequency.
         */
        private int frequency;

        // TODO implement constructor and methods
        /**
         * Contructor with String.
         * @param str String of the item.
         */
        DataItem(String str) {
            value = str;
            frequency = 1;
        }
    }
    /**
     * Find the next prime number that is larger than the new length.
     * @param n the new length before rehashing (double the old size).
     * @return n the next prime number that is larger than the new length.
     */
    private int prime(int n) {
        if (n <= 2) {
            return 2;
        }
        while (n < Integer.MAX_VALUE) {
            boolean isPrime = true;
            for (int i = 2; i < Math.sqrt(n); i++) {
                if (n % i == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                return n;
            }
            n++;
        }
        return -1;
    }
}
