/**
 * 08722 Data Structures for Application Programmers. Homework 1 MyArray.
 *
 * Andrew ID:jianrenw
 * @author: Jianren Wang
 */
public class MyArray {
    /**
     * MyArray str contains all words.
     */
    private String[] str;
    /**
     * MyArray size.
     */
    private int mySize;
    /**
     * Initialize MyArray with a given capacity.
     * @param initialCapacity initial capacity assigned to MyArray.
     * Worst-case running time complexity: O(1).
     */
    public MyArray(int initialCapacity) {
        if (initialCapacity > 0) {
            str = new String[initialCapacity];
        } else {
            str = new String[0];
        }
        mySize = 0;
    }
    /**
     * Add a validate word to the end of the array.
     * @param text given word to add.
     * Worst-case running time complexity: O(n).
     */
    public void add(String text) {
        if (text != null && text.length() != 0) {
            if (str.length == mySize) {
                if (str.length == 0) {
                    // When size is 0, plus one.
                    str = new String[1];
                } else {
                    String[] tmp = new String[str.length * 2];
                    System.arraycopy(str, 0, tmp, 0, mySize);
                    str = tmp;
                }
            }
            if (isWord(text)) {
                str[mySize] = text;
                mySize++;
            }
        }
    }
    /**
     * Search if a given word is in MyArray.
     * @param key given word to search.
     * @return true or false depends on whether the given word is in MyArray or not.
     * Worst-case running time complexity: O(n).
     */
    public boolean search(String key) {
        if (key == null || key.length() == 0) {
            return false;
        }
        for (int i = 0; i < mySize; i++) {
            if (str[i].equals(key)) {
                return true;
            }
        }
        return false;
    }
    /**
     * Get word numbers in MyArray.
     * @return word number
     * Worst-case running time complexity: O(1).
     */
    public int size() {
        return mySize;
    }
    /**
     * Get MyArray's length.
     * @return MyArray's capacity
     * Worst-case running time complexity: O(1).
     */
    public int getCapacity() {
        return str.length;
    }
    /**
     * Print all the words in one line and put a space between words.
     * Worst-case running time complexity: O(n).
     */
    public void display() {
        if (mySize > 0) {
            for (int i = 0; i < mySize - 1; i++) {
                System.out.print(str[i] + " ");
            }
            System.out.println(str[mySize - 1]);
        }
    }
    /**
     * Remove duplicates in MyArray.
     * Worst-case running time complexity: O(n^2).
     */
    public void removeDups() {
        for (int i = 0; i < mySize; i++) {
            for (int j = i + 1; j < mySize; j++) {
                if (str[j].equals(str[i])) {
                    int curr = j;
                    int next = curr + 1;
                    while (curr < mySize) {
                        str[curr] = str[next];
                        curr++;
                        next = curr + 1;
                    }
                    mySize--;
                    j--;
                }
            }
        }
    }
    /**
     * Validate a given word.
     * @param text given word to be validated.
     * @return true or false depends on whether the given word is a defined word.
     * Worst-case running time complexity: O(n).
     */
    private boolean isWord(String text) {
        return text.matches("[a-zA-Z]+");
    }
}
